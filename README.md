# 基于python语言GUI库tkinter的大学生餐厅数据库系统

#### 介绍

大三计算机系学生实验课大作业，编写一个数据库系统，要求可视化。Python+MySQL。数据库连接的华为云GuassDB，现已过期，可以修改为自己的本地数据库。

#### 软件架构

一个主页面，多个分页面，通过点击图标跳转。

#### 使用说明

代码在src文件夹中。main.py是主页面。需要修改代码中连接的数据库，然后生成exe文件。代码中用到的图片没有上传。B站有可用的exe文件的视频演示，链接：https://www.bilibili.com/video/BV1hK4y1S7Rv/

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
